﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wallpaper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Timer1_Tick(null,null);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Interval = int.Parse(txtTempo.Text)*1000;
            txtPasta.Text = System.Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + @"\Wallpapers";
            PesquisaPasta(txtPasta.Text);
            Timer1_Tick(null, null);
            //this.WindowState = FormWindowState.Minimized;
            //this.Hide();
        }

        private void PesquisaPasta(string caminho )
        {
            txtArquivos.Text = "";
            foreach (var item in Directory.GetFiles(caminho, "*.jpg", SearchOption.AllDirectories))
            {
                txtArquivos.Text += item + Environment.NewLine;
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            var aleatorio = new Random();
            var arquivo = txtArquivos.Lines[aleatorio.Next(0, txtArquivos.Lines.Length - 1)];
            
            if (Wallpaper.Set(arquivo, Wallpaper.Style.Centered))
            {
                //MessageBox.Show("Your wallpaper has been set to " + arquivo);

            }
            else
            {
                MessageBox.Show("There was a problem setting the wallpaper.");
                timer1.Enabled = false;

            }
        }

        private void txtTempo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTempo_Leave(object sender, EventArgs e)
        {
            var tempo = 0;
            int.TryParse(txtTempo.Text, out tempo);
            tempo = tempo * 1000;

            if (tempo <= 0)
            {
                tempo = 1000;
                txtTempo.Text = "1";
            }
            
            timer1.Interval =  tempo;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                //notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void txtPasta_Leave(object sender, EventArgs e)
        {
            if (Directory.Exists(txtPasta.Text))
            {
                PesquisaPasta(txtPasta.Text);
                timer1.Enabled = true;
            }
            else
            {
                timer1.Enabled = false;
            }
        }
    }
}
